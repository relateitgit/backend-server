const express = require('express');
const router = express.Router();


const sql_controller = require('../controllers/sql.controller');

/* CRUD routes for SQL */
router.post('/', sql_controller.all);
router.post('/nextService', sql_controller.nextService);

module.exports = router; 