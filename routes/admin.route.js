const express = require('express');
const router = express.Router();


const admin_controller = require('../controllers/admin.controller');

/* CRUD routes for SQL */
router.get('/shard', admin_controller.getShard);



module.exports = router; 