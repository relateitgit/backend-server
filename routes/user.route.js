const express = require('express');
const router = express.Router();


const user_controller = require('../controllers/user.controller');

/* CRUD routes for user */
router.get('/', user_controller.all);
router.get('/:id', user_controller.single_user);
router.post('/', user_controller.create);
router.post('/login', user_controller.authenticate);
router.post('/:id', user_controller.update_user);
router.delete('/:id', user_controller.delete);
module.exports = router; 