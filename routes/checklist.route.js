const express = require('express');
const router = express.Router();


const checklist_controller = require('../controllers/checklist.controller');


/* Routes for checklists, creating, deleting, updating etc. */
router.get('/', checklist_controller.all);
router.get('/:id', checklist_controller.single_checklist);
router.get('/suittype/:id', checklist_controller.suittype_checklists);
router.post('/', checklist_controller.create);
router.post('/:id', checklist_controller.update_checklist);
router.delete('/:id', checklist_controller.delete);
module.exports = router;