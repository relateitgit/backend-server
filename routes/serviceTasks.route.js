const express = require('express');
const router = express.Router();


const serviceTasks_controller = require('../controllers/serviceTasks.controller');


/* Routes for tasks, creating, deleting, updating etc. */
router.get('/', serviceTasks_controller.all);
router.get('/:id', serviceTasks_controller.single_task);
router.post('/', serviceTasks_controller.create);
router.post('/:id', serviceTasks_controller.update_task);
router.delete('/:id', serviceTasks_controller.delete);
module.exports = router;