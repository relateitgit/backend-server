const express = require('express');
const router = express.Router();


const decomission_controller = require('../controllers/decomission.controller');


/* Routes for decomissioning reasons */
router.get('/reasons', decomission_controller.all_reasons);
router.get('/reasons/:id', decomission_controller.single_reason);
router.delete('/reasons/:id', decomission_controller.delete_reason);
router.post('/reasons', decomission_controller.create_reason);

module.exports = router;