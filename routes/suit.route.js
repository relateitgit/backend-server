const express = require('express');
const router = express.Router();


const suite_controller = require('../controllers/suit.controller');

/* CRUD routes for suits */
router.get('/', suite_controller.all);
router.get('/nonDecomissioned', suite_controller.allNonDecomissioned);
router.get('/available', suite_controller.available_suits);
router.get('/:id', suite_controller.single_suit);
router.get('/:id/inUseTemp', suite_controller.inUseTempSuits);
router.get('/assignedTo/:id', suite_controller.assignedTo_suits);
router.get('/assignedToEmp/:id', suite_controller.assignedToEmployee_suits);
router.get('/:id/history', suite_controller.single_suit_history);
router.post('/', suite_controller.create);
router.post('/:id', suite_controller.update_suit);
router.post('/:id/assign', suite_controller.assign_suit);
router.post('/:id/checkout', suite_controller.checkout_suit);
router.post('/:id/updateNextService', suite_controller.updateNextService);
router.delete('/:id', suite_controller.delete);
router.delete('/', suite_controller.delete_all);
module.exports = router; // 


