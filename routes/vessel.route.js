const express = require('express');
const router = express.Router();


const vessel_controller = require('../controllers/vessel.controller');


/* Routes for locations*/
router.get('/', vessel_controller.all);
router.post('/find', vessel_controller.find_vessel);
router.get('/:id', vessel_controller.single_vessel);
router.delete('/:id', vessel_controller.delete);
router.post('/', vessel_controller.create);

module.exports = router;