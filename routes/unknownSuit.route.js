const express = require('express');
const router = express.Router();


const unknownSuit_controller = require('../controllers/unknownSuit.controller');

/* CRUD routes for unknown suits */
router.get('/', unknownSuit_controller.all);
router.get('/:id', unknownSuit_controller.single_unknownsuit);
router.post('/', unknownSuit_controller.create);
router.delete('/:id', unknownSuit_controller.delete);
module.exports = router;  