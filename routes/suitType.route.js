const express = require('express');
const router = express.Router();


const suitType_controller = require('../controllers/suitType.controller');


/* Routes for suite types e.g. vest, survival etc. */
router.get('/', suitType_controller.all);
router.get('/:id', suitType_controller.single_suitType);
router.delete('/:id', suitType_controller.delete);
router.post('/', suitType_controller.create);

module.exports = router;