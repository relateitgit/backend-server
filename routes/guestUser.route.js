const express = require('express');
const router = express.Router();


const guestUser_controller = require('../controllers/guestUser.controller');

/* CRUD routes for guest users */
router.get('/', guestUser_controller.all);
router.get('/:id', guestUser_controller.single_guestUser);
router.post('/find', guestUser_controller.find_guestUser);
router.post('/', guestUser_controller.create);
router.post('/:id', guestUser_controller.update_guestUser);
router.delete('/:id', guestUser_controller.delete);
router.delete('/internal/:id', guestUser_controller.deleteById);
router.delete('/', guestUser_controller.delete_all);
module.exports = router; 