const express = require('express');
const router = express.Router();


const upload_controller = require('../controllers/upload.controller');


/* Routes for checklists, creating, deleting, updating etc. */
router.post('/form', upload_controller.control_form);
module.exports = router;