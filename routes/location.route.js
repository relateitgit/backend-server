const express = require('express');
const router = express.Router();


const location_controller = require('../controllers/location.controller');


/* Routes for locations*/
router.get('/', location_controller.all);
router.get('/:id', location_controller.single_location);
router.delete('/:id', location_controller.delete);
router.post('/', location_controller.create);

module.exports = router;