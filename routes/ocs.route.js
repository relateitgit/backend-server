const express = require('express');
const router = express.Router();


const ocs_controller = require('../controllers/ocs.controller');

/* CRUD routes for SQL */
router.get('/:id', ocs_controller.single_employee);
router.post('/:id/addCert', ocs_controller.addCert_employee);
router.post('/:id/removeCert', ocs_controller.removeCert_employee);


module.exports = router; 