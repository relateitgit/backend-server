const express = require('express');
const router = express.Router();


const bulkCheckout_controller = require('../controllers/bulkCheckout.controller');


/* Routes for locations*/
router.get('/', bulkCheckout_controller.all);
router.get('/:id', bulkCheckout_controller.single_bulkCheckout);
router.delete('/:id', bulkCheckout_controller.delete);
router.post('/', bulkCheckout_controller.create);

module.exports = router;