const express = require('express');
const router = express.Router();


const servicePerformed_controller = require('../controllers/servicePerformed.controller');


/* Routes for service performed, creating, deleting etc. */
router.get('/', servicePerformed_controller.all);
router.get('/:id', servicePerformed_controller.single_servicePerformed);
router.get('/:id/all', servicePerformed_controller.all_servicePerformedSuit);
router.get('/:id/image', servicePerformed_controller.single_servicePerformedImage);
router.post('/', servicePerformed_controller.create);
router.delete('/:id', servicePerformed_controller.delete);
router.delete('/', servicePerformed_controller.delete_all);
module.exports = router;

