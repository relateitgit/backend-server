const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('cors');
const Schema = mongoose.Schema;
const app = express();
const port = process.env.port || 3000;
require('tls').DEFAULT_MIN_VERSION = 'TLSv1'


// Set up mongoose connection
mongoose.connect('mongodb://esva-suits-db.documents.azure.com:10255/Suits?ssl=true', {
  auth: {
    user: 'esva-suits-db',
    password: '98yTBdsM82np17ilTsQJ27Z0G63SQjbRNvvSSd2f3aWFjuTFCf1qNZaTH2qxEQFFOIlCYMUzlBEu0fLaRf5FuQ=='
  },
  useNewUrlParser: true, 
  autoIndex: false
})
mongoose.Promise = global.Promise;
let db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

// Routes
const suit = require('./routes/suit.route'); // Imports routes for the suit
const unknownSuit = require('./routes/unknownSuit.route'); // Imports routes for unknown suit
const suitType = require('./routes/suitType.route'); // Imports routes for the suitType
const serviceTasks = require('./routes/serviceTasks.route'); // Imports routes for the serviceTasks
const servicePerformed = require('./routes/servicePerformed.route'); // Imports routes for the servicePerformed
const upload = require('./routes/upload.route'); // Imports routes for upload
const checklist = require('./routes/checklist.route'); // Imports routes for the checklists
const location = require('./routes/location.route'); // Imports routes for the locations
const decomission = require('./routes/decomission.route'); // Imports routes for the decomission reasons
const user = require('./routes/user.route'); // Imports routes for the user reasons
const sql = require('./routes/sql.route'); // Imports routes for accessing the SQL
const ocs = require('./routes/ocs.route'); // Imports routes for the OCS 
const guest = require('./routes/guestUser.route'); // Imports routes for the guest users
const vessel = require('./routes/vessel.route'); // Imports routes for the vessels
const bulkCheckout = require('./routes/bulkCheckout.route'); // Imports routes for the bulk checkout
const admin = require('./routes/admin.route'); // Imports routes for the admin

// Middleware
app.use(bodyParser.json({
  limit: '50mb'
}));
app.use(bodyParser.urlencoded({
  limit: '50mb',
  extended: true
}));

// Serve static image files
app.use(express.static('images')); 

app.use(cors()); // Allow CORS 
app.options('*', cors());
app.use(function (error, req, res, next) {
  // Any request to this server will get here, and will send an HTTP
  // response with the error message 'woops'
  res.json({
    message: error.message
  });
});


app.get('/', (req, res) => res.send('Hello World!'));


app.use('/suit', suit);
app.use('/unknown', unknownSuit);
app.use('/suitType', suitType);
app.use('/decomission', decomission);
app.use('/serviceTasks', serviceTasks);
app.use('/checklist', checklist);
app.use('/location', location);
app.use('/servicePerformed', servicePerformed);
app.use('/upload', upload);
app.use('/user', user);
app.use('/ocs', sql);
app.use('/cert', ocs);
app.use('/guest', guest);
app.use('/vessel', vessel);
app.use('/bulkCheckout', bulkCheckout);
app.use('/admin', admin);


app.listen(port, () => {
  console.log('listening on ' + port)
})