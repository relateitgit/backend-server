const mongoose = require('mongoose');
var ObjectId = mongoose.Schema.Types.ObjectId;
const Schema = mongoose.Schema;

let ServicePerformedSchema = new Schema({
    suitId: {type: ObjectId},
    checklistId: {type: ObjectId},
    performedBy: {type: String},
    id: {type: String},
    tasks: {type: Array},
    image: {type:Object},
    suitSnapShot: {type:Object}
},
{
    timestamps: true
});


// Export the model
module.exports = mongoose.model('ServicePerformed', ServicePerformedSchema);