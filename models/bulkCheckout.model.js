const mongoose = require('mongoose');
var ObjectId = mongoose.Schema.Types.ObjectId;
const Schema = mongoose.Schema;

let BulkCheckoutSchema = new Schema({
    date: {type: Date},
    location: {type: ObjectId},
    performedBy: {type: Object},
    checkoutComment: {type: String},
    suits: {type: Array},
},
{
    timestamps: true
});


// Export the model
module.exports = mongoose.model('BulkCheckout', BulkCheckoutSchema);