const mongoose = require('mongoose');
diffHistory = require('mongoose-diff-history/diffHistory');
var ObjectId = mongoose.Schema.Types.ObjectId;
const Schema = mongoose.Schema;


let SuitSchema = new Schema({
    type: {type: String},
    size: {type: String},
    shoeSize: {type: String},
    id: {type: String, unique: true, required: true},
    location: {type: ObjectId}, 
    status: {type: String},
    nextService: {type: Date},
    lastService: {type: Date},
    condition: {type: String},
    assignedTo: {type: Object, default: {}},
    comment: {type: String},
    make: {type: String},
    model: {type: String},
    suitType: {type:ObjectId},
    year:{type: String},
    vessel:{type: String},
    guestSuit: {type: Boolean, default: false},
    decomissioningReason: {type: ObjectId},
    checkOutComment: {type: String},
    inUseTempComment: {type: String},
},
{
    timestamps: true
}
);

// Mongoose History Plugin
SuitSchema.plugin(diffHistory.plugin);

// Export the model
module.exports = mongoose.model('Suit', SuitSchema); 