const mongoose = require('mongoose');
diffHistory = require('mongoose-diff-history/diffHistory');
var ObjectId = mongoose.Schema.Types.ObjectId;
const Schema = mongoose.Schema;


let UnkownSuitSchema = new Schema({
    id: {type: String},
    location: {type: ObjectId}

},
{
    timestamps: true
}

);

// Mongoose History Plugin
UnkownSuitSchema.plugin(diffHistory.plugin);

// Export the model
module.exports = mongoose.model('UnknownSuit', UnkownSuitSchema); 