const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let SuitTypeSchema = new Schema({
    title: {type: String},
    tasks: { type : Array , "default" : [] },
    parts : { type : Array , "default" : [] }
});


// Export the model
module.exports = mongoose.model('SuitType', SuitTypeSchema);