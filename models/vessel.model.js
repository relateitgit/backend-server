const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let VesselSchema = new Schema({
    name: {type: String}
});


// Export the model
module.exports = mongoose.model('Vessel', VesselSchema);