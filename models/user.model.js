const mongoose = require('mongoose');
diffHistory = require('mongoose-diff-history/diffHistory');
var ObjectId = mongoose.Schema.Types.ObjectId;
const Schema = mongoose.Schema;


let UserSchema = new Schema({
    username: String,
    email: String,
    password: String,
},
{
    timestamps: true
}

);

// Mongoose History Plugin
UserSchema.plugin(diffHistory.plugin);

// Export the model
module.exports = mongoose.model('User', UserSchema); 