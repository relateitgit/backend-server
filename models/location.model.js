const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let LocationSchema = new Schema({
    title: {type: String}
});


// Export the model
module.exports = mongoose.model('Location', LocationSchema);