const mongoose = require('mongoose');
var ObjectId = mongoose.Schema.Types.ObjectId;
const Schema = mongoose.Schema;

let ChecklistSchema = new Schema({
    title: {type: String},
    tasks: { type : Array , "default" : [] },
    suitType: {type: String},
    image: {type: String},
});


// Export the model
module.exports = mongoose.model('Checklist', ChecklistSchema);