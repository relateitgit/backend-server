const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let DecomissionReasonSchema = new Schema({
    title: {type: String},
    comment: {type: String},
});


// Export the model
module.exports = mongoose.model('DecomissionReason', DecomissionReasonSchema);