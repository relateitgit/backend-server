const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let ServiceTasksSchema = new Schema({
    title: {type: String},
    input: {type: String},
    comment: {type: String},
});


// Export the model
module.exports = mongoose.model('ServiceTasks', ServiceTasksSchema);