const mongoose = require('mongoose');
diffHistory = require('mongoose-diff-history/diffHistory');
const Schema = mongoose.Schema;


let GuestUserSchema = new Schema({
    id: {type: String, unique : true},
    name: {type: String},
    phone: {type: String},
    email: {type: String},
    shoeSize:{type: String},
    size:{type: String},
    guest: {type: Boolean, default: true},
    comment: {type: String}
},
{
    timestamps: true
}

);

// Mongoose History Plugin
GuestUserSchema.plugin(diffHistory.plugin);

// Export the model
module.exports = mongoose.model('GuestUser', GuestUserSchema); 