const path = require("path");
const multer = require("multer");
var fs = require('fs');

function base64_encode(file) {
   // read binary data
   var bitmap = fs.readFileSync(file);
   // convert binary data to base64 encoded string
   return new Buffer(bitmap).toString('base64');
}

const storage = multer.diskStorage({
    destination: "./images/forms/",
    filename: function(req, file, cb){
       cb(null,"IMAGE-" + Date.now() + path.extname(file.originalname));
    }
 });

 const upload = multer({
    storage: storage,
    limits:{fileSize: 1000000},
 }).single("myImage");

// Upload control form
exports.control_form = function (req, res) { 
    upload(req, res, (err) => {
        console.log("Request ---", req.body);
        console.log("Request file ---", req.file);//Here you get file.
        /*Now do where ever you want to do*/
        if(!err)
        var base64str = base64_encode(req.file.path);
        fs.unlink(req.file.path, function (err) {            
         if (err) {                                                 
             console.error(err);                                    
         }                                                          
        console.log('File has been Deleted');                           
     });           
           return res.send(base64str).end();
     });
};


