const Vessel = require('../models/vessel.model');

// Get all vessels
exports.all = function (req, res, next) {
    Vessel.find( function (err, vessel) {
        if (err) return next(err);
        res.send(vessel);
    })
};

// Get single vessel
exports.single_vessel = function (req, res, next) {
    Vessel.findById(req.params.id, function (err, vessel) {
        if (err) return next(err);
        res.send(vessel);
    })
};

// Get vesselr by query
exports.find_vessel = function (req, res, next) {
    Vessel.find({
        "$or": [
            { name: { '$regex': req.body.value, '$options': 'i' } },
        ]
    }, function (err, vessel) {
        if (err) return next(err);
        if(!vessel){
            res.send("No matching results found.")
        } else {
            res.send(vessel);
        }
    })
};


// Create new vessel
exports.create = function (req, res, next) {
    let vessel = new Vessel(
        req.body
    );

    vessel.save(function (err) {
        if (err) {
            return next(err);
        }
        res.send('Vessel created successfully')
    })
};

// Delete single vessel
exports.delete = function (req, res) {
    Vessel.deleteOne({  _id: req.params.id }, function (err) {
        if (err) return handleError(err);
       res.send("Vessel deleted");
      });
};


