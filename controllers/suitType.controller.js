const SuitType = require('../models/suitType.model');

// Get all types
exports.all = function (req, res, next) {
    SuitType.find( function (err, suitType) {
        if (err) return next(err);
        res.send(suitType);
    })
};

// Get single type
exports.single_suitType = function (req, res, next) {
    SuitType.findById(req.params.id, function (err, suitType) {
        if (err) return next(err);
        res.send(suitType);
    })
};


// Create new type
exports.create = function (req, res, next) {
    let suitType = new SuitType(
        req.body
    );

    suitType.save(function (err) {
        if (err) {
            return next(err);
        }
        res.send('Suittype created successfully')
    })
};

// Delete single type
exports.delete = function (req, res, next) {
    SuitType.deleteOne({  _id: req.params.id }, function (err) {
        if (err) return handleError(err);
       res.send("Type deleted");
      });
};


