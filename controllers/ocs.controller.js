const axios = require('axios');
const CircularJSON = require('circular-json');
process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';
var moment = require('moment');


// Get single OCS employee
exports.single_employee = function (req, res, next) {
    const id = req.params.id;
    axios.post("https://satseaocs.esvagt.com:8099/restws/services/token", "UserName=SaatSea&Password=OCSSaatSea1122&grant_type=password")
        .then((result) => {
            const token = "Bearer " + result.data.access_token;
            console.log(token);
            axios.get("https://satseaocs.esvagt.com:8099/restws/services/saatsea/personnel/" + id, {
                    'headers': {
                        'Authorization': token
                    }
                })
                .then((result) => {
                    res.send(result.data);
                }).catch((error) => {
                    console.log(error);
                });



        }).catch((error) => {
            console.log(error);
        });

};

// Add certificate to employee
exports.addCert_employee = function (req, res, next) {
    const id = req.params.id;
    axios.post("https://satseaocs.esvagt.com:8099/restws/services/token", "UserName=SaatSea&Password=OCSSaatSea1122&grant_type=password")
        .then((result) => {
            const token = "Bearer " + result.data.access_token;
            // INSERT REQUEST
            const payload = {
                "ImportSource": "WRITEAPI",
                "ExternalPersonId": id, // Employee id
                "CertificateNumber": req.body.suitId, // Suit id
                "Certificate": "DRAGT",
                "ValidFrom": moment().format('DD.MM.YYYY').toString(), // Date.now()
                "ValidTo": moment(req.body.validTo).format('DD.MM.YYYY').toString(), // Next service date
            }

            axios.put("https://satseaocs.esvagt.com:8099/ocs.api.services/ImportService/addCertificateHeld", payload)
                .then((result) => {
                    console.log(result);
                    res.send(result.data)

                }).catch((error) => {
                    console.log(error);
                    res.status(500).send({ message: error.toString()})

                });

        }).catch((error) => {
            console.log(error);
            res.status(500).send({ message: error.toString()})

        });

};

// Remove certificate from employee
exports.removeCert_employee = function (req, res, next) {
  
    const id = req.params.id;
    axios.post("https://satseaocs.esvagt.com:8099/restws/services/token", "UserName=SaatSea&Password=OCSSaatSea1122&grant_type=password")
        .then((result) => {
            const token = "Bearer " + result.data.access_token;
            // INSERT REQUEST
            const payload = {
                "ImportSource": "WRITEAPI",
                "ExternalPersonId": id, // Employee id
                "CertificateNumber": "", // Suit id
                "Certificate": "DRAGT",
                // "Remarks": "Test",
                "ValidTo": moment().add(100, 'y').format('DD.MM.YYYY').toString(), // Next service date
            }
            // HØJDE:   VÆGT:   DRAGT/SKO: S 41  ÅRGANG: 2016  TYPE:
            axios.put("https://satseaocs.esvagt.com:8099/ocs.api.services/ImportService/addCertificateHeld", payload)
                .then((result) => {
                    console.log(result);
                    res.send(result.data);

                }).catch((error) => {
                    console.log(error); 
                });

        }).catch((error) => {
            console.log(error);
        });

};