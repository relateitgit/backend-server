const ServicePerformed = require('../models/servicePerformed.model');
const fs = require('fs');
const uuidv4 = require('uuid/v4');

function base64_encode(file) {
    // read binary data
    var bitmap = fs.readFileSync(file);
    // convert binary data to base64 encoded string
    return new Buffer(bitmap).toString('base64');
 }

// Get all services performed
exports.all = function (req, res) {
    ServicePerformed.find( function (err, servicePerformed, next) {
        if (err) return next(err);
        res.send(servicePerformed);
    }).select("-image");
};

// Get single service performed
exports.single_servicePerformed = function (req, res, next) {
    ServicePerformed.findById(req.params.id, function (err, servicePerformed) {
        if (err) return next(err);
        res.send(servicePerformed);
    });
};

// Get all services performed for one suit
exports.all_servicePerformedSuit = function (req, res, next) {
    ServicePerformed.find({"id" : req.params.id}, function (err, servicePerformed) {
        if (err) return next(err);
        res.send(servicePerformed);
    }).select("-image");
};

// Get single service performed
exports.single_servicePerformedImage = function (req, res, next) {
    ServicePerformed.findById(req.params.id, function (err, servicePerformed) {
        if (err) return next(err);
        res.send(servicePerformed);
    })
};

// Create new service performed
exports.create = function (req, res, next) {

    function decodeBase64Image(dataString) {
        var matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/),
          response = {};
      
        if (matches.length !== 3) {
          return new Error('Invalid input string');
        }
      
        response.type = matches[1];
        response.data = new Buffer(matches[2], 'base64');
      
        return response;
      }
      


   

    if (req.body.image){
    const imageBuffer = decodeBase64Image(req.body.image);

    const fileName = './images/services/'+ uuidv4() +'.png';
    fs.writeFile( fileName, imageBuffer.data, function(err) { 
        if (err) return console.error(err); 
        var base64str = base64_encode(fileName);
        fs.unlink(fileName, function (err) {            
         if (err) {                                                 
             console.error(err);                                    
         }    
        })
        req.body.image = base64str;
    
        let servicePerformed = new ServicePerformed(
            req.body
        );
        
        servicePerformed.save(function (err) {
            if (err) {
                return next(err);
            }
            res.send("Services has been created");
        })
    });
} else {
    let servicePerformed = new ServicePerformed(
        req.body
    );
    servicePerformed.save(function (err) {
        if (err) {
            return next(err);
        }
        res.send("Services has been created");
    })
}
    

    /*
   
    */
};


// Delete single service performed
exports.delete = function (req, res, next) { 
    ServicePerformed.deleteOne({  _id: req.params.id }, function (err) {
        if (err) return handleError(err);
       res.send("Service Performed deleted");
      });
};


// Delete all service performed
exports.delete_all = function (req, res, next) { 
    ServicePerformed.remove({}, function(err) {
        if (err) {
            console.log(err)
        } else {
            res.end('success');
        }
    }
);
};
