const Location = require('../models/location.model');

// Get all locations
exports.all = function (req, res, next) {
    Location.find( function (err, location) {
        if (err) return next(err);
        res.send(location);
    })
};

// Get single location
exports.single_location = function (req, res, next) {
    Location.findById(req.params.id, function (err, location) {
        if (err) return next(err);
        res.send(location);
    })
};


// Create new location
exports.create = function (req, res, next) {
    let location = new Location(
        req.body
    );

    location.save(function (err) {
        if (err) {
            return next(err);
        }
        res.send('Location created successfully')
    })
};

// Delete single location
exports.delete = function (req, res) {
    Location.deleteOne({  _id: req.params.id }, function (err) {
        if (err) return handleError(err);
       res.send("Location deleted");
      });
};


