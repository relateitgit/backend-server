const Checklist = require('../models/checklist.model');

// Get all checklists
exports.all = function (req, res) {
    Checklist.find( function (err, checklist, next) {
        if (err) return next(err);
        res.send(checklist);
    }).select("-image")
};

// Get single checklist
exports.single_checklist = function (req, res, next) {
    Checklist.findById(req.params.id, function (err, checklist) {
        if (err) return next(err);
        res.send(checklist);
    })
};

// Get checklist for suittype
exports.suittype_checklists = function (req, res, next) {
    Checklist.find({suitType:req.params.id}, function (err, checklist) {
        if (err) return next(err);
        res.send(checklist);
    })
};

// Create new checklist
exports.create = function (req, res, next) {
    let checklist = new Checklist(
        req.body
    );

    checklist.save(function (err) {
        if (err) {
            return next(err);
        }
        res.send(checklist)
    })
};

// Update checklist
exports.update_checklist = function (req, res, next) {
    Checklist.updateOne({ _id: req.params.id, }, { $set: req.body}, function (err, checklist) {
        if (err) return next(err);
        res.send("Checklist has been updated");
    })
};

// Delete single checklist
exports.delete = function (req, res, next) { 
    Checklist.deleteOne({  _id: req.params.id }, function (err) {
        if (err) return handleError(err);
       res.send("Checklist deleted");
      });
};
