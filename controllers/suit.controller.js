const Suit = require('../models/suit.model');
const diffHistory = require('mongoose-diff-history/diffHistory');
var moment = require('moment');
const axios = require('axios');

// Get all suits
exports.all = function (req, res, next) {
    Suit.find(function (err, suits) {
        if (err) return next(err);
        res.send(suits);
    })
};

// Get all suits that are not decomissioned
exports.allNonDecomissioned = function (req, res, next) {
    Suit.find().where("status").ne("Decomissioned").exec(function (err, suits) {
        if (err) return next(err);
        res.send(suits);
    });
};

exports.inUseTempSuits = function (req, res, next) {
    Suit.find().where("inUseTempComment").eq(req.params.id).exec(function (err, suits) {
        if (err) return next(err);
        res.send(suits);
    });
};

// Get available suits
exports.available_suits = function (req, res, next) {
    Suit.find().where("assignedTo").in(["", null]).exec(function (err, suits) {
        if (err) return next(err);
        res.send(suits);
    });
};

// Get single suit
exports.single_suit = function (req, res, next) {
    Suit.findOne({
        id: req.params.id
    }, function (err, suit) {
        if (err) return next(err);
        if (!suit) {
            res.send("No matching results found.")
        } else {
            res.send(suit);
        }
    })
};

// Get suits by assigned to
exports.assignedTo_suits = function (req, res, next) {
    Suit.find({
        "assignedTo.id": req.params.id
    }, function (err, suit) {
        if (err) return next(err);
        if (!suit) {
            res.send("No matching results found.")
        } else {
            res.send(suit);
        }
    })
};

// Get suits by assigned to
exports.assignedToEmployee_suits = function (req, res, next) {
    Suit.find({
        "assignedTo.EmployeeID": req.params.id
    }, function (err, suit) {
        if (err) return next(err);
        if (!suit) {
            res.send("No matching results found.")
        } else {
            res.send(suit);
        }
    })
};



// Get single suit history
exports.single_suit_history = function (req, res) {
    diffHistory.getDiffs('Suit', req.params.id)
        .then(histories => {
            res.send(histories);
        })
        .catch(console.error);
};

// Create new suit
exports.create = function (req, res) {

    // If location is other than IN-USE TEMP inUseTempComment has to be cleared
    if (req.body.inUseTempComment && req.body.location !== "5c769806982d4a7969a3859b") {
        req.body.inUseTempComment = "";
    }
    let suit = new Suit(
        req.body
    );

    suit.save(function (err, suit) {
        if (err) {
            res.send(err);
        }
        res.send(suit)
    })
};

// Update suit
exports.update_suit = function (req, res, next) {

    // If location is other than IN-USE TEMP inUseTempComment has to be cleared
    if (req.body.inUseTempComment && req.body.location !== "5c769806982d4a7969a3859b") {
        req.body.inUseTempComment = "";
    }

    Suit.updateOne({
        id: req.params.id,
    }, {
        $set: req.body
    }, function (err, suit) {
        if (err) return next(err);
        res.send("Suit has been updated");
    })
};

// Assign suit
exports.assign_suit = function (req, res, next) {
    console.log(req.body)
    Suit.updateOne({
        id: req.params.id,
    }, {
        assignedTo: req.body
    }, function (err, suit) {
        if (err) return next(err);
        res.send("Suit has been assigned");
    })
};

// Update next service date
exports.updateNextService = function (req, res, next) {
    const nextService =  moment(req.body.newNextService).add(1, 'y');
    const payload = {
            suitId: req.body.suit.id,
            validTo: nextService
        }
    axios.post('http://localhost:3000/cert/' + req.body.user.EmployeeID + '/addCert', payload)
    Suit.updateOne({
        id: req.params.id,
    }, {
        nextService: nextService
    }, function (err, suit) {
        if (err) return next(err);
        res.send("Next service has been updated");
    })
};

// Checkout suit
exports.checkout_suit = function (req, res, next) {

    // If location is other than IN-USE TEMP inUseTempComment has to be cleared
    if (req.body.inUseTempComment && req.body.location !== "5c769806982d4a7969a3859b") {
        req.body.inUseTempComment = "";
    }

    Suit.updateOne({
        id: req.params.id,
    }, {
        location: req.body.location,
        checkOutComment: req.body.checkOutComment,
        inUseTempComment: req.body.inUseTempComment
    }, function (err, suit) {
        if (err) return next(err);
        res.send("Suit has been checked out");
    })
};

// Delete single suite
exports.delete = function (req, res, next) {
    Suit.findOneAndDelete({
        id: req.params.id
    }, function (err) {
        if (err) return next(err);
        res.send("Suite with id " + req.params.id + " deleted");
    });
};

// Delete all suits
exports.delete_all = function (req, res, next) {
    Suit.remove({}, function (err) {
        if (err) {
            console.log(err)
        } else {
            res.end('success');
        }
    });
};