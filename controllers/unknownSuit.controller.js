const UnknownSuits = require('../models/unknownSuit.model');


// Get all unknown suits
exports.all = function (req, res, next) {
    UnknownSuits.find(function (err, unknownSuit) {
        if (err) return next(err);
        res.send(unknownSuit);
    })
};

// Get single unknown suit
exports.single_unknownsuit = function (req, res, next) {
    UnknownSuits.findOne({
        id: req.params.id
    }, function (err, unknownSuit) {
        if (err) return next(err);
        if (!suit) {
            res.send("No matching results found.")
        } else {
            res.send(unknownSuit);
        }

    })
};



// Create new unknown suit
exports.create = function (req, res) { 
    let unknownSuit = new UnknownSuits(
        req.body
    );

    unknownSuit.save(function (err, unknownSuit) { 
        if (err) {
            res.send(err);
        }
        res.send(unknownSuit)
    })
};


// Delete single unknown suit
exports.delete = function (req, res, next) {
    UnknownSuits.findOneAndDelete({
        id: req.params.id
    }, function (err) {
        if (err) return next(err);
        res.send("Unknown suit with id " + req.params.id + " deleted");
    });
};