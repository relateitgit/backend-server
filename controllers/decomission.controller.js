const DecomissionReason = require('../models/decomissionReason.model');

// Get all decomissioning reasons
exports.all_reasons = function (req, res, next) {
    DecomissionReason.find( function (err, location) {
        if (err) return next(err);
        res.send(location);
    })
};

// Get single decomissioning reason
exports.single_reason = function (req, res, next) {
    DecomissionReason.findById(req.params.id, function (err, location) {
        if (err) return next(err);
        res.send(location);
    })
};


// Create new decomissioning reason
exports.create_reason = function (req, res, next) {
    let reason = new DecomissionReason(
        req.body
    );

    reason.save(function (err) {
        if (err) {
            return next(err);
        }
        res.send('Reason created successfully')
    })
};

// Delete single decomissioning reason
exports.delete_reason = function (req, res) {
    DecomissionReason.deleteOne({  _id: req.params.id }, function (err) {
        if (err) return handleError(err);
       res.send("Reason deleted");
      });
};


