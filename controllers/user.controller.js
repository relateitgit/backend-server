const User = require('../models/user.model');

// Get all users
exports.all = function (req, res, next) {
    User.find( function (err, user) {
        if (err) return next(err);
        res.send(user);
    })
};

// Get single user
exports.single_user = function (req, res, next) {
    User.findOne({id: req.params.id}, function (err, user) {
        if (err) return next(err);
        if(!user){
            res.send("No matching results found.")
        } else {
            res.send(user);
        }
        
    })
};


// Authenticate
exports.authenticate = function (req, res, next) {
    User.findOne({username: req.body.username, password: req.body.password}, function (err, user) {
        if (err) return next(err);
        if(!user){
            res.send("Wrong username or password.")
        } else {
            res.send("Login success");
        }
        
    })
};

// Create new user
exports.create = function (req, res) { 
    let user = new User(
        req.body
    );

    user.save(function (err, user, next) { 
        if (err) {
            return next(err);
        }
        res.send(user)
    })
};

// Update user
exports.update_user = function (req, res, next) {
    User.updateOne({ id: req.params.id, }, { $set: req.body}, function (err, user) {
        if (err) return next(err);
        res.send("User has been updated");
    })
};

// Delete single user
exports.delete = function (req, res, next) {
    User.findOneAndDelete({  id: req.params.id }, function (err) {
        if (err) return next(err);
       res.send("User with id " + req.params.id +" deleted");
      });
};
