const GuestUser = require('../models/guestUser.model');
const diffHistory = require('mongoose-diff-history/diffHistory');

// Get all guest user
exports.all = function (req, res, next) {
    GuestUser.find( function (err, GuestUsers) {
        if (err) return next(err);
        res.send(GuestUsers);
    })
};

// Get single guest user
exports.single_guestUser = function (req, res, next) {
    GuestUser.findOne({id: req.params.id}, function (err, guestUser) {
        if (err) return next(err);
        if(!guestUser){
            res.send("No matching results found.")
        } else {
            res.send(guestUser);
        }
    })
};

// Get guest user by query
exports.find_guestUser = function (req, res, next) {
    GuestUser.find({
        "$or": [
            { name: { '$regex': req.body.value, '$options': 'i' } },
            { id: { '$regex': req.body.value, '$options': 'i' } }
        ]
    }, function (err, guestUser) {
        if (err) return next(err);
        if(!guestUser){
            res.send("No matching results found.")
        } else {
            res.send(guestUser);
        }
    })
};


// Create new guest user
exports.create = function (req, res) { 
    

    GuestUser.findOne({id: req.body.id}, function (err, guestUser) {
        
        if (err) return next(err);
        if(!guestUser){
            let newGuestUser = new GuestUser(
                req.body
            );
        
            newGuestUser.save(function (err, guestUser) { 
                if (err) {
                    res.send(err);
                }
                res.send(guestUser)
            })
        } else {
            res.status(400).send('User with ID already exists');
        }
    })

    
};

// Update guest user
exports.update_guestUser = function (req, res, next) {
    
    GuestUser.updateOne({ id: req.params.id, }, { $set: req.body}, function (err, guestUser) {
        if (err) return next(err);
        res.send("Guest user has been updated");
    })
};



// Delete single guest user
exports.delete = function (req, res, next) {
    GuestUser.findOneAndDelete({  id: req.params.id }, function (err) {
        if (err) return next(err);
       res.send("Guest user with id " + req.params.id +" deleted");
      });
};

// Delete single guest user by internal id
exports.deleteById = function (req, res, next) {
    GuestUser.findOneAndDelete({  _id: req.params.id }, function (err) {
        if (err) return next(err);
       res.send("Guest user with id " + req.params.id +" deleted");
      });
};

// Delete all guest users
exports.delete_all = function (req, res, next) { 
    GuestUser.remove({}, function(err) {
        if (err) {
            console.log(err)
        } else {
            res.end('success');
        }
    }
);
};
