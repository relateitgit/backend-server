const BulkCheckout = require('../models/bulkCheckout.model');

// Get all checkouts
exports.all = function (req, res, next) {
    BulkCheckout.find( function (err, bulkCheckout) {
        if (err) return next(err);
        res.send(bulkCheckout);
    })
};

// Get single bulk checkout
exports.single_bulkCheckout = function (req, res, next) {
    BulkCheckout.findById(req.params.id, function (err, bulkCheckout) {
        if (err) return next(err);
        res.send(bulkCheckout);
    })
};



// Create new bulk checkout
exports.create = function (req, res, next) {
    let bulkCheckout = new BulkCheckout(
        req.body
    );

    bulkCheckout.save(function (err) {
        if (err) {
            return next(err);
        }
        res.send('Checkout created successfully')
    })
};

// Delete single bulk checkout
exports.delete = function (req, res) {
    BulkCheckout.deleteOne({  _id: req.params.id }, function (err) {
        if (err) return handleError(err);
       res.send("Checkout deleted");
      });
};


