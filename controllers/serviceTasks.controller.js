const ServiceTasks = require('../models/serviceTasks.model');

// Get all tasks
exports.all = function (req, res) {
    ServiceTasks.find( function (err, task, next) {
        if (err) return next(err);
        res.send(task);
    })
};

// Get single task
exports.single_task = function (req, res) {
    ServiceTasks.findById(req.params.id, function (err, task, next) {
        if (err) return next(err);
        res.send(task);
    })
};

// Create new task
exports.create = function (req, res) {
    let task = new ServiceTasks(
        {
            title: req.body.title,
            comment: req.body.comment,
        }
    );

    task.save(function (err) {
        if (err) {
            return next(err);
        }
        res.send('Task created successfully')
    })
};

// Update task
exports.update_task = function (req, res) {
    ServiceTasks.updateOne({ _id: req.params.id, }, { $set: req.body}, function (err, suit) {
        if (err) return next(err);
        res.send("Task has been updated");
    })
};

// Delete single task
exports.delete = function (req, res) {
    ServiceTasks.deleteOne({  _id: req.params.id }, function (err) {
        if (err) return handleError(err);
       res.send("Tasks deleted");
      });
};
