const sql = require('mssql');

// DB Config
var config = {
    user: 'SQL_CrewReadOnly',
    password: '7AWe%H7xDt%u',
    server: '10.188.0.5',
    database: 'edw'
};

// Get users
exports.all = function (req, res) {
    sql.connect(config).then(pool => {
        // Stored procedure

        return pool.request()
            .input('String', sql.NVarChar(30), req.body.value)
            .execute('stpEmployeeSearch').then(function (result, recordsets, returnValue, affected) {
                res.send(result.recordsets[0]);
                sql.close();
            }).catch(function (err) {
                console.log(err);
                res.send(err);
                sql.close();
            });
    })


};



// Get next servive
exports.nextService = function (req, res) {
    sql.connect(config).then(pool => {
        // Stored procedure

        return pool.request()
            .input('EmployeeID', sql.NVarChar(30), req.body.value)
            .execute('stpEmployeeNextAssignment').then(function (result, recordsets, returnValue, affected) {
                res.send(result.recordsets[0]);
                sql.close();
            }).catch(function (err) {
                console.log(err);
                res.send(err);
                sql.close();
            });
    })


};


/*
Gæstebruger info
Alle samme størrelse som fra SQL
Navn
Email
ID nummer
Telefonnummer

*/